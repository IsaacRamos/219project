package MapEditor.model;

import javafx.scene.control.SelectionModel;
import javafx.scene.image.ImageView;

/**
 * Created by KaiserFeuer on 7/12/2016.
 */
public class MovableImage extends ImageView {
    private double x;
    private double y;


    public MovableImage(){
        super();
        setOnMousePressed(e->{
            x = getX()-e.getX();
            y = getY()-e.getY();

        });
        setOnMouseDragged(e->{
            setX(e.getX()+x);
            setY(e.getY()+y);
        });


    }
}
