package MapEditor.model;

import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.effect.InnerShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by KaiserFeuer on 7/13/2016.
 */
public abstract class SelectableGroup extends Group {
    private SelectionModel<Node> selectionModel;
    private Effect effect;


    protected SelectableGroup() {
        super();

        selectionModel = new SingleSelectionModel<Node>() {
            @Override
            protected Node getModelItem(int index) {
                if (index >= 0 && index < getChildren().size())
                    return getChildren().get(index);
                return null;
            }

            @Override
            protected int getItemCount() {
                return getChildren().size();
            }
        };
        selectionModel.selectedItemProperty().addListener(e->{
            if(selectionModel.getSelectedItem()==null){
                for(Node node : getChildren())
                    node.setEffect(null);
            }

        });
    }

    public void add(Node node) {
        getChildren().add(node);
        node.setOnMouseClicked(e -> {
            if (e.getClickCount() == 1) {
                {
                    select(((Node) e.getSource()));
                }
            }
            onMouseClickedHook(e);
        });
    }

    public void select(Node source) {
        Node current = selectionModel.getSelectedItem();
        if (current != null) {
            current.setEffect(null);
        }
        source.setEffect(effect);

        selectionModel.select(source);
    }

    public void select(int index) {
        if(index>=0&&index<getChildren().size())
            select(getChildren().get(index));

    }

    public SelectionModel<Node> getSelectionModel() {
        return selectionModel;
    }

    public void setSelectionEffect(Effect effect) {
        this.effect = effect;
    }

    public void remove(){
        getChildren().remove(selectionModel.getSelectedItem());
        selectionModel.select(null);
    }

    public void clearSelection(){
        selectionModel.select(null);
    }

    public abstract void onMouseClickedHook(MouseEvent e);

}
