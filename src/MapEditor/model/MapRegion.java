package MapEditor.model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.ColorPicker;
import javafx.scene.paint.Color;

/**
 * Model class for the Regions of the Regio Vinco Map Editor
 * Created by Isaac on 6/28/2016.
 */
public class MapRegion {
    private SimpleStringProperty name = new SimpleStringProperty("");
    private SimpleStringProperty leader = new SimpleStringProperty("");
    private SimpleStringProperty capital = new SimpleStringProperty("");
    private double[][] subregions;
    private ColorPicker regionColor;

    public MapRegion(String name, String leader, String capital) {
        regionColor = new ColorPicker();
        this.name.set(name);
        this.capital.set(capital);
        this.leader.set(leader);
    }

    public MapRegion(){
        this("","","");
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getLeader() {
        return leader.get();
    }

    public SimpleStringProperty leaderProperty() {
        return leader;
    }

    public void setLeader(String leader) {
        this.leader.set(leader);
    }

    public String getCapital() {
        return capital.get();
    }

    public SimpleStringProperty capitalProperty() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital.set(capital);
    }

    public double[][] getSubregions() {
        return subregions;
    }



    public void setSubregions(double[][] subregions) {
        this.subregions = subregions;
    }

    public Color getRegionColor() {
        return regionColor.getValue();
    }

    public ObjectProperty colorProperty(){
        return regionColor.valueProperty();
    }
    public void setRegionColor(String regionColor) {
        this.regionColor.setValue(Color.web(regionColor));
    }
    public void setRegionColor(Color regionColor) {
        this.regionColor.setValue(regionColor);
    }

}
