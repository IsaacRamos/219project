package MapEditor.model;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.util.Pair;

import java.util.ArrayList;

/**
 * Data Manager for the MapEditor app
 * Created by isramos on 7/7/2016.
 */
public class MapData {
    private SimpleStringProperty name;
    private Color backgroundColor;
    private Color borderColor;
    private SimpleDoubleProperty borderWidth;
    private SimpleDoubleProperty zoom;
    private String parentDirectory;
    private double mapHeight;
    private double mapWidth;
    private String geometryFile;
    private SimpleDoubleProperty translateX;
    private SimpleDoubleProperty translateY;
    private MapRegion[] subregions;

    private ArrayList<ImageView> images;
    private boolean edited;

    public MapData(){
        name = new SimpleStringProperty("");
        translateX = new SimpleDoubleProperty();
        translateY = new SimpleDoubleProperty();
        borderWidth = new SimpleDoubleProperty();
        zoom = new SimpleDoubleProperty();
        images = new ArrayList<>(2);
    }

    public double getZoom() {
        return zoom.get();
    }

    public SimpleDoubleProperty zoomProperty() {
        return zoom;
    }

    public void setZoom(double zoom) {
        this.zoom.set(zoom);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = Color.web(backgroundColor);
    }
    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public Color getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(String borderColor) {
        this.borderColor = Color.web(borderColor);
    }
    public void setBorderColor(Color borderColor) {
        this.borderColor = borderColor;
    }

    public double getBorderWidth() {
        return borderWidth.get();
    }

    public SimpleDoubleProperty borderWidthProperty() {
        return borderWidth;
    }

    public void setBorderWidth(double borderWidth) {
        this.borderWidth.set(borderWidth);
    }



    public MapRegion[] getSubregions() {
        return subregions;
    }

    public void markAsEdited(){
        edited = true;
    }

    public String getParentDirectory() {
        return parentDirectory;
    }

    public void setParentDirectory(String parentDirectory) {
        this.parentDirectory = parentDirectory;
    }

    public void setSubregions(MapRegion[] subregions) {
        this.subregions = subregions;
    }

    public double getMapHeight() {
        return mapHeight;
    }

    public void setMapHeight(double mapHeight) {
        this.mapHeight = mapHeight;
    }

    public double getMapWidth() {
        return mapWidth;
    }

    public void setMapWidth(double mapWidth) {
        this.mapWidth = mapWidth;
    }

    public String getGeometryFile() {
        return geometryFile;
    }

    public void setGeometryFile(String geometryFile) {
        this.geometryFile = geometryFile;
    }


    public ArrayList<ImageView> getImages() {
        return images;
    }

    public boolean isEdited() {
        return edited;
    }

    public void setEdited(boolean edited) {
        this.edited = edited;
    }

    public double getTranslateX() {
        return translateX.get();
    }

    public SimpleDoubleProperty translateXProperty() {
        return translateX;
    }

    public void setTranslateX(double translateX) {
        this.translateX.set(translateX);
    }

    public double getTranslateY() {
        return translateY.get();
    }

    public SimpleDoubleProperty translateYProperty() {
        return translateY;
    }

    public void setTranslateY(double translateY) {
        this.translateY.set(translateY);
    }

    public void clear(){
        images.clear();

    }
}
