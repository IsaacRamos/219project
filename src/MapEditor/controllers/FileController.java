package MapEditor.controllers;

import javax.json.*;
import javax.json.stream.JsonGenerator;
import javax.json.stream.JsonParser;
import java.io.*;

import MapEditor.model.MapData;
import MapEditor.model.MapRegion;
import MapEditor.model.MovableImage;
import MapEditor.view.ProgressDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.util.Pair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by isramos on 7/7/2016.
 */
public class FileController {
    private static final String NUM_SUBREGIONS = "NUMBER_OF_SUBREGIONS";
    private static final String SUBREGIONS_ARRAY = "SUBREGIONS";
    private static final String NUM_POLYGONS = "NUMBER_OF_SUBREGION_POLYGONS";
    private static final String POLYGONS = "SUBREGION_POLYGONS";
    private static final String X_COORDINATE = "X";
    private static final String Y_COORDINATE = "Y";

    MapData data;

    /**
     * Default Constructor
     *
     * @param data instance of the Model class to load the data on
     */
    public FileController(MapData data) {
        this.data = data;
    }

    /**
     * Loads MapData values with the data on file
     *
     * @param jsonFilePath the path on the file
     * @throws IOException is file does not exist
     */
    public void load(String jsonFilePath) throws IOException {
        JsonObject root = loadJsonFile(jsonFilePath);


        loadWorkspace(root);



        loadMap(root.getString("subregions"));


        loadMapRegions(root);



        loadImages(root);


    }

    /**
     *
     * @param filePath
     * @throws IOException
     */
    public void save(String filePath) throws IOException {
        //
        String name = data.getName();
        String bgColor = toHex(data.getBackgroundColor());
        String borderColor = toHex(data.getBorderColor());
        double borderWidth = data.getBorderWidth();
        double zoom = data.getZoom();
        String parent = data.getParentDirectory();
        double width = data.getMapWidth();
        double height = data.getMapHeight();
        String geometryFile = data.getGeometryFile();
        double translateX = data.getTranslateX();
        double translateY = data.getTranslateY();
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

        for(MapRegion e : data.getSubregions()){
            JsonObject object = Json.createObjectBuilder()
                    .add("subregion_name",e.getName())
                    .add("subregion_leader",e.getLeader())
                    .add("subregion_capital",e.getCapital())
                    .add("subregion_color",toHex(e.getRegionColor())).build();
            arrayBuilder.add(object);
        }
        JsonArrayBuilder images = Json.createArrayBuilder();
        for(ImageView e : data.getImages()){
            JsonObject object = Json.createObjectBuilder()
                    .add("path",e.getId())
                    .add("x",e.getX())
                    .add("y",e.getY())
                    .build();
            images.add(object);
        }
        JsonArray regions = arrayBuilder.build();
        JsonObject root = Json.createObjectBuilder()
                .add("name",name).add("background_color",bgColor)
                .add("border_color",borderColor)
                .add("border_width",borderWidth)
                .add("zoom",zoom)
                .add("height",height)
                .add("width",width)
                .add("translateX",translateX)
                .add("translateY",translateY)
                .add("parent_directory",parent)
                .add("subregions",geometryFile)
                .add("subregions_data",regions)
                .add("images",images)
                .build();

        JsonToFile(root,filePath);

    }

    public void export() throws IOException{
        File file = new File(data.getParentDirectory()+"/"+data.getName());
        if(!file.isDirectory())
            if(!file.mkdir())
                file.mkdirs();
       addValues();






    }

    private void addValues() throws IOException{
        String name = data.getName();
        boolean leader =true;
        boolean flag =true;
        boolean capital = true;
        JsonArrayBuilder array = Json.createArrayBuilder();
        String path = "work/" +data.getName()+"/";
        File file;
        for(MapRegion e:data.getSubregions()){
            file = new File(path+e.getLeader()+ ".png");
            if (e.getLeader().isEmpty()|| !file.exists())
                leader =false;
            if(e.getCapital().isEmpty()){
                capital = false;
            }
            file = new File(path+e.getName()+" Flag.png");
            if(!file.exists()){
                flag = false;
            }
        }
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        for (MapRegion e : data.getSubregions()){
            JsonObjectBuilder object = Json.createObjectBuilder();
            object.add("name",e.getName());
            if(leader)
                object.add("leader",e.getLeader());
            if(capital)
                object.add("capital",e.getCapital());
            object.add("red", (int) ((e.getRegionColor().getRed())*255));
            object.add("green", (int) ((e.getRegionColor().getGreen())*255));
            object.add("blue", (int) ((e.getRegionColor().getBlue())*255));
            arrayBuilder.add(object.build());
        }
        JsonObject root = Json.createObjectBuilder()
                .add("name",data.getName())
                .add("subregions_have_capitals",capital)
                .add("subregions_have_flags",flag)
                .add("subregions_have_leaders",leader)
                .add("subregions",arrayBuilder)
                .build();
        String filepath = data.getParentDirectory()+"/"+data.getName()+"/"+data.getName()+".rvm";
        JsonToFile(root,filepath);

    }

    private void loadWorkspace(JsonObject root) {

        data.setName(root.getString("name"));
        data.setBackgroundColor(root.getString("background_color"));
        data.setBorderColor(root.getString("border_color"));
        data.setGeometryFile(root.getString("subregions"));
        data.setBorderWidth(getDataAsDouble(root, "border_width"));
        data.setZoom(getDataAsDouble(root, "zoom"));
        data.setMapHeight(getDataAsDouble(root,"height"));
        data.setMapWidth(getDataAsDouble(root,"width"));
        data.setParentDirectory(root.getString("parent_directory"));
        data.setTranslateX(getDataAsDouble(root,"translateX"));
        data.setTranslateY(getDataAsDouble(root,"translateY"));


    }

    private void loadImages(JsonObject root) {
        JsonArray jsonArray = root.getJsonArray("images");
        for (int i = 0; i < jsonArray.size(); i++) {
            JsonObject object = jsonArray.getJsonObject(i);
            String path = object.getString("path");
            File file = new File(path);
            ImageView imageView = new MovableImage();
            imageView.setImage(new Image(file.toURI().toString()));
            imageView.setX(getDataAsDouble(object, "x"));
            imageView.setY(getDataAsDouble(object, "y"));
            imageView.setId(path);
            data.getImages().add(imageView);
        }
    }

    public void loadMap(String jsonFilePath) throws IOException {
        JsonObject jsonObject = loadJsonFile(jsonFilePath);
        int numberOfSubregions = getDataAsInt(jsonObject, NUM_SUBREGIONS);
        JsonArray jsonSubRegions = jsonObject.getJsonArray(SUBREGIONS_ARRAY);

        MapRegion[] regions = new MapRegion[numberOfSubregions];
        double[][] subregions;


        for (int i = 0; i < numberOfSubregions; i++) {
            JsonObject jsonSubregion = jsonSubRegions.getJsonObject(i);
            int numberOfPolygons = getDataAsInt(jsonSubregion, NUM_POLYGONS);
            JsonArray jsonPolygons = jsonSubregion.getJsonArray(POLYGONS);
            subregions = new double[numberOfPolygons][];
            for (int j = 0; j < numberOfPolygons; j++) {
                JsonArray jsonSubPolygons = jsonPolygons.getJsonArray(j);
                double[] points = new double[jsonSubPolygons.size() * 2];
                for (int k = 0; k < jsonSubPolygons.size(); k++) {
                    JsonObject jsonPolygon = jsonSubPolygons.getJsonObject(k);
                    points[k * 2] = (getDataAsDouble(jsonPolygon, X_COORDINATE) + 180) / 360.0;
                    points[k * 2 + 1] = (getDataAsDouble(jsonPolygon, Y_COORDINATE) * -1 + 90) / 180.0;
                }
                subregions[j] = points;
            }
            regions[i] = new MapRegion();
            regions[i].setSubregions(subregions);
        }
        data.setSubregions(regions);

    }

    private void loadMapRegions(JsonObject root) {
        JsonArray regions = root.getJsonArray("subregions_data");
        JsonObject map;
        for (int i = 0; i < regions.size(); i++) {
            map = regions.getJsonObject(i);
            data.getSubregions()[i].setName(map.getString("subregion_name"));
            data.getSubregions()[i].setCapital(map.getString("subregion_capital"));
            data.getSubregions()[i].setLeader(map.getString("subregion_leader"));
            data.getSubregions()[i].setRegionColor(map.getString("subregion_color"));
        }

    }

    private double getDataAsDouble(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber) value;
        return number.bigDecimalValue().doubleValue();
    }

    private int getDataAsInt(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber) value;
        return number.bigIntegerValue().intValue();
    }


    private JsonObject loadJsonFile(String jsonFilePath) throws IOException {

        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;

    }

    private void JsonToFile(JsonObject root,String filePath) throws IOException{
        HashMap<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(root);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);

        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(root);
        String prettyPrinted = sw.toString();
        PrintWriter pw =  new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
    }

    private static String toHex(Color c){
        String hex = String.format( "#%02X%02X%02X",
                (int)( c.getRed() * 255 ),
                (int)( c.getGreen() * 255 ),
                (int)( c.getBlue() * 255 ) );
        return hex;

    }



}
