package MapEditor.controllers;

import MapEditor.model.MapData;
import MapEditor.model.MapRegion;
import MapEditor.model.MovableImage;
import MapEditor.model.SelectableGroup;
import MapEditor.view.DimensionDialog;
import MapEditor.view.NewDialog;
import MapEditor.view.ProgressDialog;
import MapEditor.view.SubRegionDialog;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.IntegerBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableArray;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.*;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;
import javafx.scene.transform.Scale;
import javafx.scene.transform.Transform;
import javafx.scene.transform.Translate;
import javafx.stage.FileChooser;
import javafx.util.Pair;

import javax.imageio.ImageIO;
import javax.json.stream.JsonParsingException;
import javax.sound.midi.*;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.*;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Controller Class for the Regio Vinco Application
 */
public class MapEditorController implements Initializable {


    //Strings
    public static final String playImage = "MapEditor/rsc/images/play.png";
    public static final String pauseImage = "MapEditor/rsc/images/pause.png";

    //Resources
    ResourceBundle bundle;
    private static final String base = "MapEditor.rsc.bundles.MapEditor";
    //Containers
    @FXML
    BorderPane workspace; //root
    @FXML
    Pane mapPane;
    @FXML
    ColorPicker bgPicker;
    @FXML
    ColorPicker borderPicker;
    @FXML
    Slider borderSlider;
    @FXML
    Slider zoomSlider;
    @FXML
    TableView mapTable;
    @FXML
    TabPane tabPane;
    @FXML
    ScrollPane scrollPane;

    //FXML Controls
    @FXML
    Button playButton;
    @FXML
    ImageView playImageView;
    ImageView pauseImageView;
    @FXML
    TextField mapName;
    @FXML
    Tab editTab;


    ObservableList<MapRegion> regionList;
    @FXML
    ProgressBar progress;

    //Instances

    MapData mapData;
    FileController fileController;

    SelectableGroup polygons;
    SelectableGroup images;
    Task task;

    private boolean done;

    SubRegionDialog subregion;
    ChangeListener selection;
    Sequencer player;


    @Override
    public void initialize(URL location, ResourceBundle resources) {


        bundle = ResourceBundle.getBundle(base);
        regionList = FXCollections.observableList(new ArrayList<>());
        mapTable.setItems(regionList);
        pauseImageView = new ImageView(new Image(getClass().getClassLoader().getResource(pauseImage).toExternalForm()));
        InnerShadow effect = new InnerShadow(BlurType.GAUSSIAN, Color.YELLOW, 0, 0, 0, 0);
        polygons = new SelectableGroup() {
            @Override
            public void onMouseClickedHook(MouseEvent e) {
                if (e.getClickCount() == 1) {
                    mapTable.scrollTo(polygons.getSelectionModel().getSelectedIndex());
                    mapTable.getSelectionModel().select(polygons.getSelectionModel().getSelectedIndex());
                    mapTable.getFocusModel().focus(polygons.getSelectionModel().getSelectedIndex());
                }
                if (e.getClickCount() > 1) {
                    edit(polygons.getSelectionModel().getSelectedIndex());
                }
            }
        };
        polygons.setSelectionEffect(effect);
        images = new SelectableGroup() {
            @Override
            public void onMouseClickedHook(MouseEvent e) {

            }
        };
        images.setSelectionEffect(new DropShadow(2, Color.YELLOW));
        mapData = new MapData();
        fileController = new FileController(mapData);
        mapPane.setPrefSize(802, 536);
        mapPane.setClip(new Rectangle(802, 536));

    }

    private void setup() {

        ObjectProperty<Background> background = mapPane.backgroundProperty();
        background.bind(Bindings.createObjectBinding(() -> {
                    BackgroundFill fill = new BackgroundFill(bgPicker.getValue(), CornerRadii.EMPTY, Insets.EMPTY);
                    return new Background(fill);
                }, bgPicker.valueProperty()
        ));

        scrollPane.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.UP) {
                polygons.setTranslateY(polygons.getTranslateY() - 0.05 * mapData.getMapHeight());
            }
            if (e.getCode() == KeyCode.DOWN) {
                polygons.setTranslateY(polygons.getTranslateY() + 0.05 * mapData.getMapHeight());
            }
            if (e.getCode() == KeyCode.RIGHT) {
                polygons.setTranslateX(polygons.getTranslateX() + 0.05 * mapData.getMapWidth());
            }
            if (e.getCode() == KeyCode.LEFT) {
                polygons.setTranslateX(polygons.getTranslateX() - 0.05 * mapData.getMapWidth());
            }
        });
        setupTable();


    }

    private void edit(int selectedIndex) {
        subregion.setSelectedIndex(selectedIndex);
        subregion.show();
    }

    /**
     * TODO
     *
     * @param actionEvent
     */
    public void exit(ActionEvent actionEvent) {
        System.exit(0);
    }

    /**
     * TODO
     *
     * @param actionEvent
     */
    public void play(ActionEvent actionEvent) {
        if (player != null && player.isRunning()) {
            player.stop();
            playButton.setGraphic(playImageView);

        } else {
            File file = new File("work/" + mapName.getText() + "/" + mapName.getText() + " National Anthem.mid");
            if (file.exists()) {
                try {
                    Sequence sequence = MidiSystem.getSequence(file);
                    player = MidiSystem.getSequencer();
                    player.open();
                    player.setSequence(sequence);
                    player.start();
                    playButton.setGraphic(pauseImageView);
                } catch (InvalidMidiDataException e) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle(bundle.getString("error"));
                    alert.setContentText(bundle.getString("play-error"));
                    alert.show();
                } catch (IOException e) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle(bundle.getString("error"));
                    alert.setContentText(bundle.getString("play-error"));
                    alert.show();
                } catch (MidiUnavailableException e) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle(bundle.getString("error"));
                    alert.setContentText(bundle.getString("play-error"));
                    alert.show();
                }
            }
        }
    }

    /**
     * TODO
     *
     * @param actionEvent
     */
    public void load(ActionEvent actionEvent) {


        FileChooser chooser = new FileChooser();
        chooser.setTitle("Load New Map");
        File file = new File("work");


        if (!file.isDirectory()) {
            if (!file.mkdir())
                file.mkdirs();
        }
        chooser.setInitialDirectory(file);
        final File file1 = chooser.showOpenDialog(workspace.getScene().getWindow());
        if (file1 != null) {
            try {
                clear();
                fileController.load(file1.getPath());


                loadWorkspace();


            } catch (IOException e) {

                Alert error = new Alert(Alert.AlertType.ERROR);
                error.setTitle(bundle.getString("error"));
                error.setContentText(bundle.getString("load-error"));
                error.show();


            } catch (JsonParsingException e) {

                Alert error = new Alert(Alert.AlertType.ERROR);
                error.setTitle(bundle.getString("error"));
                error.setContentText(bundle.getString("invalid-json"));
                error.show();
            }


        }
    }

    private void loadWorkspace() {

        regionList.addAll(mapData.getSubregions());

        int count = 0;
        for (MapRegion e : mapData.getSubregions()) {
            Group group = new Group();
            for (double[] b : e.getSubregions()) {
                for (int i = 0; i < b.length / 2; i++) {
                    b[2 * i] *= mapData.getMapWidth();
                    b[2 * i + 1] *= mapData.getMapHeight();
                }
                Polygon polygon = new Polygon(b);

                polygon.setStrokeType(StrokeType.OUTSIDE);
                polygon.fillProperty().bind(e.colorProperty());
                polygon.strokeProperty().bind(borderPicker.valueProperty());
                polygon.strokeWidthProperty().bind(borderSlider.valueProperty());

                group.setId("" + count);
                group.getChildren().add(polygon);
            }
            polygons.add(group);
            count++;

        }
        zoomSlider.setValue(mapData.getZoom());
        mapData.zoomProperty().bind(zoomSlider.valueProperty());
        borderSlider.setValue(mapData.getBorderWidth());
        mapData.borderWidthProperty().bind(borderSlider.valueProperty());
        polygons.scaleXProperty().bind(zoomSlider.valueProperty());
        polygons.scaleYProperty().bind(zoomSlider.valueProperty());
        polygons.setTranslateX(mapData.getTranslateX());
        polygons.setTranslateY(mapData.getTranslateY());
        mapData.translateXProperty().bind(polygons.translateXProperty());
        mapData.translateYProperty().bind(polygons.translateYProperty());
        mapPane.getChildren().addAll(polygons);
        bgPicker.setValue(mapData.getBackgroundColor());
        borderPicker.setValue(mapData.getBorderColor());

        for (ImageView e : mapData.getImages())
            images.add(e);
        mapPane.getChildren().add(images);
        mapPane.setPrefSize(mapData.getMapWidth(),mapData.getMapHeight());
        subregion = new SubRegionDialog(mapData);
        mapName.setText(mapData.getName());
        editTab.setDisable(false);
        setup();


    }


    public void export(ActionEvent actionEvent) {
        try {
            clearSelection();
            fileController.export();
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle(bundle.getString("error"));
            alert.setContentText(bundle.getString("export-error"));
            alert.show();
        }


        SnapshotParameters param = new SnapshotParameters();
        param.setViewport(new Rectangle2D(0, 0, mapData.getMapWidth(), mapData.getMapHeight()));
        Translate translate = new Translate();
        translate.setX(-mapPane.getLayoutX());
        translate.setY(-mapPane.getLayoutY());
        param.setTransform(translate);
        WritableImage image = mapPane.snapshot(param, null);
        File file = new File(mapData.getParentDirectory()+"/"+mapData.getName()+"/"+mapData.getName()+".png");
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle(bundle.getString("success"));
            alert.setContentText(bundle.getString("export-success"));
            alert.show();
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle(bundle.getString("error"));
            alert.setContentText(bundle.getString("export-error"));
            alert.show();
        }


    }

    public void clear() {

        mapData.borderWidthProperty().unbind();
        polygons.scaleXProperty().unbind();
        polygons.scaleYProperty().unbind();
        mapData.zoomProperty().unbind();
        mapData.translateXProperty().unbind();
        mapData.translateYProperty().unbind();

        images.getChildren().clear();
        polygons.getChildren().clear();
        mapPane.getChildren().clear();
        regionList.clear();
        mapData.clear();
        mapTable.setRowFactory(null);
        if (selection != null)
            mapTable.getFocusModel().focusedIndexProperty().removeListener(selection);

    }

    public void addImage(ActionEvent actionEvent) {

        FileChooser choose = new FileChooser();
        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("Image File", "*.png");
        choose.setTitle(bundle.getString("image-title"));
        choose.getExtensionFilters().add(filter);
        File file = choose.showOpenDialog(workspace.getScene().getWindow());
        if (file != null) {
            File file1 = new File("work/" + mapData.getName() + "/" + file.getName());
            if (!file1.isDirectory()) {
                if (!file1.mkdir()) {
                    file1.mkdirs();
                }
            }
            try {
                Path path = Files.copy(file.toPath(), file1.toPath(), StandardCopyOption.REPLACE_EXISTING);
                Image image = new Image(path.toUri().toString());
                ImageView view = new MovableImage();
                view.setImage(image);
                view.setId(file1.toString());

                mapData.getImages().add(view);
                images.add(view);
            } catch (IOException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle(bundle.getString("error"));
                alert.setContentText(bundle.getString("add-error"));
                alert.show();
            }

        }

    }

    public void removeImage(ActionEvent actionEvent) {
        if (images.getSelectionModel().getSelectedItem() != null) {
            images.remove();
        }
        //TODO
    }

    private void setupTable() {

        mapTable.setRowFactory(param -> {
            TableRow<MapRegion> row = new TableRow<MapRegion>();
            row.setOnMouseClicked(e -> {
                if (row.getIndex() >= 0 && row.getIndex() < regionList.size()) {
                    if (e.getClickCount() == 1) {
                        polygons.select(mapTable.getSelectionModel().getSelectedIndex());
                        mapTable.getFocusModel().focus(mapTable.getSelectionModel().getSelectedIndex());
                    }
                    if (e.getClickCount() > 1) {
                        if (mapTable.getSelectionModel().getSelectedItem() != null) {
                            edit(mapTable.getSelectionModel().getSelectedIndex());
                        }
                    }
                }
            });
            return row;
        });
        selection = new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                polygons.select(mapTable.getFocusModel().getFocusedIndex());
            }
        };
        mapTable.getFocusModel().focusedIndexProperty().addListener(selection);
    }


    public void newMap(ActionEvent actionEvent) {
        NewDialog dialog = new NewDialog();
        Optional<ButtonType> result = dialog.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            mapName.setText(dialog.getNameField().getText());
            mapData.setParentDirectory(dialog.getParentField().getText());
            mapData.setBackgroundColor("#FFFFFF");
            mapData.setBorderColor("#000000");
            mapData.setMapHeight(536);
            mapData.setMapWidth(802);
            mapData.setBorderWidth(0.1);
            mapData.setGeometryFile(dialog.getFileField().getText());
            mapData.setZoom(1);
            mapData.setName(mapName.getText());
            try {
                fileController.loadMap(dialog.getFileField().getText());
                int spacing = ((int) Math.floor(253.0 / mapData.getSubregions().length));
                int gray = 1;
                for (MapRegion e : mapData.getSubregions()) {
                    e.setRegionColor(Color.grayRgb(gray));
                    gray += spacing;

                }
                Platform.runLater(() -> loadWorkspace());
            } catch (IOException ex) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle(bundle.getString("error"));
                alert.setContentText(bundle.getString("load-error"));
                alert.show();
            }

        }

    }

    public void resize(ActionEvent actionEvent) {
        DimensionDialog dialog = new DimensionDialog();
        dialog.getHeightField().setText(Double.toString(mapData.getMapHeight()));
        dialog.getWidthField().setText(Double.toString(mapData.getMapWidth()));
        dialog.showAndWait().filter(b -> b == ButtonType.OK).ifPresent(e -> {
            mapData.setMapWidth(Double.parseDouble(dialog.getWidthField().getText()));
            mapData.setMapHeight(Double.parseDouble(dialog.getHeightField().getText()));
            mapPane.setPrefSize(mapData.getMapWidth(),mapData.getMapHeight());
            mapPane.setClip(new Rectangle(mapData.getMapWidth(),mapData.getMapHeight()));
        });
    }

    public void save(ActionEvent actionEvent) {
        mapData.setBorderColor(borderPicker.getValue());
        mapData.setBackgroundColor(bgPicker.getValue());
        if (!mapName.getText().equals(mapData.getName())) {
            File file = new File("work/" + mapData.getName() + "/" + mapData.getName() + ".json");
            if (file.exists()) {
                file.delete();
                file = file.getParentFile();
                file.renameTo(new File("work/" + mapName.getText()));
            }
            mapData.setName(mapName.getText());

        }
        try {
            File file = new File("work/" + mapData.getName());
            if (!file.isDirectory())
                if (!file.mkdir())
                    file.mkdirs();
            fileController.save("work/" + mapData.getName() + "/" + mapData.getName() + ".json");
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle(bundle.getString("success"));
            alert.setContentText(bundle.getString("save-success") + " work/" + mapData.getName());
            alert.show();
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle(bundle.getString("error"));
            alert.setContentText(bundle.getString("save-error"));
            alert.show();
        }
    }

    public void changeColors(ActionEvent actionEvent) {
        int index = (int) (Math.random() * mapData.getSubregions().length);
        int val = 1;
        int step = (((int) Math.floor(253 / mapData.getSubregions().length)));
        for (int i = 0; i < mapData.getSubregions().length; i++, val += step) {
            mapData.getSubregions()[(i + index) % mapData.getSubregions().length].setRegionColor(Color.grayRgb(val));
        }


    }

    public void clearSelection() {
        mapTable.getSelectionModel().clearSelection();
        images.clearSelection();
        polygons.clearSelection();
    }
}
