package MapEditor;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Map Editor for Regio Vinco
 */
public class MapEditorApp extends Application {
    private static final String appIcon = "MapEditor/rsc/images/MapEditor.png";
    @Override
    public void start(Stage primaryStage) throws Exception{
        ResourceBundle bundle = ResourceBundle.getBundle("MapEditor.rsc.bundles.MapEditor", Locale.getDefault());
        Parent root = FXMLLoader.load(getClass().getResource("view/MapEditorView.fxml"),bundle);
        primaryStage.setTitle("Map Editor");
        primaryStage.setScene(new Scene(root, 1280, 720));
        URL url = this.getClass().getClassLoader().getResource(appIcon);
        String path = url.toExternalForm();
        Image icon = new Image(path);
        primaryStage.getIcons().add(icon);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
