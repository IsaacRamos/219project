package MapEditor.test_bed;

import MapEditor.controllers.FileController;
import MapEditor.model.MapData;
import MapEditor.model.MapRegion;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.util.Pair;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Isaac Ramos on 7/10/2016.
 */
public class FileControllerSlovakiaTest extends JavafxViewTest{
    MapData mapData;
    FileController file;
    @Before
    public void setUp() throws Exception {
        mapData = new MapData();
        createSlovakia();
        file = new FileController(mapData);

    }

    private void createSlovakia() {

        mapData.setName("Slovakia");
        mapData.setParentDirectory("The World/Europe");
        mapData.setGeometryFile("raw_map_data/Slovakia.json");
        mapData.setBackgroundColor("#DC6E01");
        mapData.setBorderColor("#000000");
        mapData.setBorderWidth(.1);
        mapData.setZoom(50);
        mapData.setMapWidth(802);
        mapData.setMapHeight(536);
        mapData.setTranslateX(0);
        mapData.setTranslateY(0);

        String path1 = "work/The World/Europe/Slovakia/symbol.png";


        ImageView image1 = new ImageView();
        image1.setX(24);
        image1.setY(32);
        String path2 = "work/The World/Europe/Slovakia/flag.png";
        image1.setId(path1);
        ImageView image2 = new ImageView();
        image2.setX(543);
        image2.setY(369);
        image2.setId(path2);


        mapData.getImages().add(image1);
        mapData.getImages().add(image2);


        MapRegion[] mapRegions = new MapRegion[8];


        mapRegions[0] = new MapRegion("Bratislava", "", "");
        mapRegions[0].setRegionColor(Color.grayRgb(250));

        mapRegions[1] = new MapRegion("Trnava", "", "");
        mapRegions[1].setRegionColor(Color.grayRgb(249));

        mapRegions[2] = new MapRegion("Trencin", "", "");
        mapRegions[2].setRegionColor(Color.grayRgb(248));

        mapRegions[3] = new MapRegion("Nitra", "", "");
        mapRegions[3].setRegionColor(Color.grayRgb(247));

        mapRegions[4] = new MapRegion("Zilina", "","");
        mapRegions[4].setRegionColor(Color.grayRgb(246));

        mapRegions[5]= new MapRegion("Banska Bystrica", "", "");
        mapRegions[5].setRegionColor(Color.grayRgb(245));

        mapRegions[6] = new MapRegion("Presov", "","");
        mapRegions[6].setRegionColor(Color.grayRgb(244));

        mapRegions[7] = new MapRegion("Kosice","","");
        mapRegions[7].setRegionColor(Color.grayRgb(243));

        mapData.setSubregions(mapRegions);

    }

    @Test
    public void saveAndLoad() throws Exception {
        this.file.save("work/The World/Europe/Slovakia/Slovakia.json");
        load();
        same();
    }

    public void load() throws Exception {
        file.load("work/The World/Europe/Slovakia/Slovakia.json");
    }


    public void same() throws Exception {
        Assert.assertEquals("Slovakia",mapData.getName());
        Assert.assertEquals((19.86818504333496+180)/360,mapData.getSubregions()[0].getSubregions()[0][0],1E-18);
        Assert.assertEquals((49.19557189941406*-1 +90)/180,mapData.getSubregions()[0].getSubregions()[0][1],1E-18);
        Assert.assertEquals("Zilina",mapData.getSubregions()[4].getName());
        Assert.assertEquals("Bratislava",mapData.getSubregions()[0].getName());
        for (MapRegion e:
                mapData.getSubregions()) {
            Assert.assertNotNull(e);
        }
    }



}