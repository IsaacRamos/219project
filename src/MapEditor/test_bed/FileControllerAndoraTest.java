package MapEditor.test_bed;

import MapEditor.controllers.FileController;
import MapEditor.model.MapData;
import MapEditor.model.MapRegion;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.util.Pair;
import org.junit.*;

import java.io.IOException;

/**
 * Created by Isaac Ramos on 7/10/2016.
 */
public class FileControllerAndoraTest extends JavafxViewTest{
    MapData mapData;
    FileController file;

    @Before
    public void setUp() throws Exception {
        mapData = new MapData();
        createAndorra(mapData);
        file = new FileController(mapData);



    }
    private static void createAndorra(MapData mapData) throws IOException {

        mapData.setName("Andorra");
        mapData.setParentDirectory("The World/Europe");
        mapData.setGeometryFile("raw_map_data/Andorra.json");
        mapData.setBackgroundColor("#DC6E00");
        mapData.setBorderColor("#000000");
        mapData.setBorderWidth(.1);
        mapData.setZoom(200);
        mapData.setMapWidth(802);
        mapData.setMapHeight(536);
        mapData.setTranslateX(0);
        mapData.setTranslateY(0);

        String path1 = "work/Andorra/symbol.png";


        ImageView image1 = new ImageView();
        image1.setX(7);
        image1.setY(9);
        String path2 = "work/Andorra/flag.png";
        image1.setId(path1);
        ImageView image2 = new ImageView();
        image2.setX(580);
        image2.setY(390);
        image2.setId(path2);

        mapData.getImages().add(image1);
        mapData.getImages().add(image2);


        MapRegion[] mapRegions = new MapRegion[7];


        mapRegions[0] = new MapRegion("Ordino", "Ventura Espot", "Oridino (Town)");
        mapRegions[0].setRegionColor(Color.grayRgb(200));

        mapRegions[1] = new MapRegion("Canillo", "Enric Casadevall Medrano", "Canillo (Town)");
        mapRegions[1].setRegionColor(Color.grayRgb(198));

        mapRegions[2] = new MapRegion("Encamp", "Miquel Alis Font", "Encamp (Town)");
        mapRegions[2].setRegionColor(Color.grayRgb(196));

        mapRegions[3] = new MapRegion("Escaldes-Engordany", "Montserrat Capdevila Pallarés", "Escaldes-Engordany (town)");
        mapRegions[3].setRegionColor(Color.grayRgb(194));

        mapRegions[4] = new MapRegion("La Massana","Josep Areny", "La Massana (town)");
        mapRegions[4].setRegionColor(Color.grayRgb(192));

        mapRegions[5]= new MapRegion("Andorra la Vella", "Maria Rosa Ferrer Obiols", "Andorra la Vella (city)");
        mapRegions[5].setRegionColor(Color.grayRgb(190));

        mapRegions[6] = new MapRegion("Sant Julia de Loria","Josep Pintat Forné", "Sant Julia de Loria (town)");
        mapRegions[6].setRegionColor(Color.grayRgb(188));

        mapData.setSubregions(mapRegions);



    }



    @Test
    public void saveAndLoad() throws Exception {
        this.file.save("work/Andorra/Andorra.json");
        load();
        same();
    }

    public void load() throws Exception {
        file.load("work/Andorra/Andorra.json");
        System.out.println();
    }


    public void same() throws Exception {
        Assert.assertEquals("Andorra",mapData.getName());
        Assert.assertEquals((1.5205550193787758+180)/360,mapData.getSubregions()[0].getSubregions()[0][0],1E-18);
        Assert.assertEquals((42.483692169189396*-1 +90)/180,mapData.getSubregions()[0].getSubregions()[0][1],1E-18);
        Assert.assertEquals("La Massana",mapData.getSubregions()[4].getName());
        Assert.assertEquals("Ventura Espot",mapData.getSubregions()[0].getLeader());
        for (MapRegion e:
             mapData.getSubregions()) {
            Assert.assertNotNull(e);
        }
    }
}