package MapEditor.test_bed;

import MapEditor.controllers.FileController;
import MapEditor.model.MapData;
import MapEditor.model.MapRegion;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

/**
 * Created by KaiserFeuer on 7/10/2016.
 */
public class FileControllerLoadTest extends JavafxViewTest {
    MapData data;
    FileController file;
    @Before
    public void setUp() throws Exception {
        data = new MapData();
        file = new FileController(data);


    }

    @After
    public void tearDown() throws Exception {
        System.out.println(data.getName());
        System.out.println(data.getBackgroundColor().toString());
        System.out.println(data.getBorderWidth());
        System.out.println(data.getGeometryFile());
        System.out.println(data.getParentDirectory());
        for(MapRegion e : data.getSubregions()){
            System.out.println(e.getName());
            System.out.println(e.getCapital());
            System.out.println(e.getRegionColor());
            System.out.println(e.getSubregions()[0][0] +"" +e.getSubregions()[0][1]);
        }
    }

    @Test
    public void load() throws Exception {
        file.load("work/The World/Europe/Andorra/Andorra.json");

    }

}