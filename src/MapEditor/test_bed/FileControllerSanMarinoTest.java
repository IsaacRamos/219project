package MapEditor.test_bed;

import MapEditor.controllers.FileController;
import MapEditor.model.MapData;
import MapEditor.model.MapRegion;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.util.Pair;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by KaiserFeuer on 7/11/2016.
 */
public class FileControllerSanMarinoTest extends JavafxViewTest {
    MapData mapData;
    FileController file;
    @Before
    public void setUp() throws Exception {
        mapData = new MapData();
        createSanMarino();
        file = new FileController(mapData);


    }

    private void createSanMarino() {
        mapData.setName("San Marino");
        mapData.setParentDirectory("The World/Europe");
        mapData.setGeometryFile("raw_map_data/San Marino.json");
        mapData.setBackgroundColor("#DC6E01");
        mapData.setBorderColor("#000000");
        mapData.setBorderWidth(.1);
        mapData.setZoom(200);
        mapData.setMapWidth(802);
        mapData.setMapHeight(536);
        mapData.setTranslateX(0);
        mapData.setTranslateY(0);



        MapRegion[] mapRegions = new MapRegion[9];


        mapRegions[0] = new MapRegion("Acquaviva",  "Lucia Tamagnini","");
        mapRegions[0].setRegionColor(Color.grayRgb(225));

        mapRegions[1] = new MapRegion("Borgo Maggiore", "Sergio Nanni","");
        mapRegions[1].setRegionColor(Color.grayRgb(200));

        mapRegions[2] = new MapRegion("Chiesanuova","Franco Santi","");
        mapRegions[2].setRegionColor(Color.grayRgb(175));

        mapRegions[3] = new MapRegion("Domagnano",  "Gabriel Guidi","");
        mapRegions[3].setRegionColor(Color.grayRgb(150));

        mapRegions[4] = new MapRegion("Faetano","Pier Mario Bedetti","");
        mapRegions[4].setRegionColor(Color.grayRgb(125));

        mapRegions[5]= new MapRegion("Fiorentino", "Gerri Fabbri","");
        mapRegions[5].setRegionColor(Color.grayRgb(100));

        mapRegions[6] = new MapRegion("Montegiardino","Marta Fabbri","");
        mapRegions[6].setRegionColor(Color.grayRgb(75));

        mapRegions[7] = new MapRegion("City of San Marino","Maria Teresa Beccario","");
        mapRegions[7].setRegionColor(Color.grayRgb(50));

        mapRegions[8] = new MapRegion("Serravalle","Leandro Maiani","");
        mapRegions[8].setRegionColor(Color.grayRgb(25));

        mapData.setSubregions(mapRegions);

    }

    @Test
    public void saveAndLoad() throws Exception {
        this.file.save("work/The World/Europe/San Marino/San Marino.json");
        load();
        same();
    }

    public void load() throws Exception {
        file.load("work/The World/Europe/San Marino/San Marino.json");
    }


    public void same() throws Exception {
        Assert.assertEquals("San Marino",mapData.getName());
        Assert.assertEquals((12.428207397460938+180)/360,mapData.getSubregions()[0].getSubregions()[0][0],1E-18);
        Assert.assertEquals((43.94343185424805*-1 +90)/180,mapData.getSubregions()[0].getSubregions()[0][1],1E-18);
        Assert.assertEquals("Faetano",mapData.getSubregions()[4].getName());
        Assert.assertEquals("Lucia Tamagnini",mapData.getSubregions()[0].getLeader());
        for (MapRegion e:
                mapData.getSubregions()) {
            Assert.assertNotNull(e);
        }
    }

}