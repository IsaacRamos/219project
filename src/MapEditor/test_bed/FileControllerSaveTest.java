package MapEditor.test_bed;

import MapEditor.controllers.FileController;
import MapEditor.model.MapData;
import MapEditor.model.MapRegion;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.util.Pair;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.*;

/**
 * Created by Isaac Ramos on 7/10/2016.
 */
public class FileControllerSaveTest {

    MapData mapData;
    FileController file;
    @Before
    public void setUp() throws Exception {
        mapData = new MapData();
        createAndorra();

    }

    private void createAndorra() throws IOException {

        mapData.setName("Andorra");
        mapData.setParentDirectory("The World/Europe");
        mapData.setGeometryFile("raw_map_data/Andorra.json");
        mapData.setBackgroundColor("#DC6E00");
        mapData.setBorderColor("#000000");
        mapData.setBorderWidth(0.1);
        mapData.setZoom(200);
        mapData.setMapWidth(802);
        mapData.setMapHeight(536);
        mapData.setTranslateX(0);
        mapData.setTranslateY(0);

        String path1 = "work/The World/Europe/Andorra/symbol.png";


        ImageView image1 = new ImageView();
        image1.setX(7);
        image1.setY(9);
        String path2 = "work/The World/Europe/Andorra/flag.png";
        image1.setId(path1);
        ImageView image2 = new ImageView();
        image2.setX(580);
        image2.setY(390);
        image2.setId(path2);

        mapData.getImages().add(image1);
        mapData.getImages().add(image2);
        file = new FileController(mapData);

        MapRegion[] mapRegions = new MapRegion[7];


        mapRegions[0] = new MapRegion("Ordino", "Ventura Espot", "Oridino (Town)");
        mapRegions[0].setRegionColor(Color.grayRgb(200));

        mapRegions[1] = new MapRegion("Canillo", "Enric Casadevall Medrano", "Canillo (Town)");
        mapRegions[1].setRegionColor(Color.grayRgb(198));

        mapRegions[2] = new MapRegion("Encamp", "Miquel Alis Font", "Encamp (Town)");
        mapRegions[2].setRegionColor(Color.grayRgb(196));

        mapRegions[3] = new MapRegion("Escaldes-Engordany", "Montserrat Capdevila Pallarés", "Escaldes-Engordany (town)");
        mapRegions[3].setRegionColor(Color.grayRgb(194));

        mapRegions[4] = new MapRegion("La Massana","Josep Areny", "La Massana (town)");
        mapRegions[4].setRegionColor(Color.grayRgb(192));

        mapRegions[5]= new MapRegion("Andorra la Vella", "Maria Rosa Ferrer Obiols", "Andorra la Vella (city)");
        mapRegions[5].setRegionColor(Color.grayRgb(190));

        mapRegions[6] = new MapRegion("Sant Julia de Loria","Josep Pintat Forné", "Sant Julia de Loria (town)");
        mapRegions[6].setRegionColor(Color.grayRgb(188));

        mapData.setSubregions(mapRegions);

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void save() throws Exception {

        this.file.save("work/The World/Europe/Andorra/Andorra.json");

    }

}