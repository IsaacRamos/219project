package MapEditor.view;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by Isaac on 7/4/2016.
 */
public class DimensionDialog extends Dialog {
    private static final String base = "MapEditor.rsc.bundles.MapEditor";
    private static final String style = "MapEditor/rsc/css/Dialog.css";
    private TextField widthField;
    private TextField heightField;

    private ResourceBundle bundle;

    public DimensionDialog(){
        getDialogPane().getScene().getStylesheets().add(this.getClass().getClassLoader().getResource(style).toExternalForm());
        bundle = ResourceBundle.getBundle(base, Locale.getDefault());
        setTitle(bundle.getString("dimension-dialog-title"));
        GridPane pane = new GridPane();

        //Labels
        Label width = new Label(bundle.getString("width"));
        Label height = new Label(bundle.getString("height"));

        //Controls
        widthField = new TextField("");
        widthField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d*\\.")) {
                    widthField.setText(newValue.replaceAll("[^\\d\\.]", ""));
                }
            }
        });
        heightField = new TextField("");
        heightField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d*\\.")) {
                    heightField.setText(newValue.replaceAll("[^\\d\\.]", ""));
                }
            }
        });

        pane.add(width,0,0);
        pane.add(height,0,1);
        pane.add(widthField,1,0);
        pane.add(heightField,1,1);

        pane.setHgap(10);
        pane.setVgap(10);

        getDialogPane().getButtonTypes().addAll(ButtonType.OK,ButtonType.CANCEL);
        getDialogPane().setContent(pane);

    }

    public TextField getWidthField() {
        return widthField;
    }

    public TextField getHeightField() {
        return heightField;
    }
}
