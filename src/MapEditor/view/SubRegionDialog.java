package MapEditor.view;

import MapEditor.model.MapData;
import MapEditor.model.MapRegion;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;

import java.io.File;
import java.nio.file.Files;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by Isaac on 7/4/2016.
 */
public class SubRegionDialog extends Dialog {
    //Test

    Image image = new Image(this.getClass().getClassLoader().getResource(defaultImage).toExternalForm());
    boolean toggle;

    //
    private static final String baseName = "MapEditor.rsc.bundles.MapEditor";
    private static final String defaultImage = "MapEditor/rsc/images/default.png";
    private static final String prevImage = "MapEditor/rsc/images/prev.png";
    private static final String nextImage = "MapEditor/rsc/images/next.png";
    private static final String style = "MapEditor/rsc/css/Dialog.css";
    private ResourceBundle bundle;
    ImageView flagImage;
    ImageView leaderImage;
    TextField nameField;
    TextField leaderField;
    TextField capitalField;
    Button nextButton;
    Button prevButton;

    private MapData data;
    MapRegion[] subregions;

    private int selectedIndex;
    private SimpleIntegerProperty indexProperty;
    File file;


    public SubRegionDialog(MapData data) {
        this.data = data;
        this.subregions = data.getSubregions();
        indexProperty = new SimpleIntegerProperty(0);

        getDialogPane().setPrefSize(700,450);

        this.getDialogPane().getScene().getStylesheets().add(getClass().getClassLoader().getResource(style).toExternalForm());
        bundle = ResourceBundle.getBundle(baseName, Locale.getDefault());
        VBox vBox = new VBox();
        vBox.getStyleClass().add("vbox");
        BorderPane borderPane = new BorderPane();
        HBox hBox = new HBox();
        hBox.getStyleClass().add("hbox");

        ImageView next = new ImageView(new Image(this.getClass().getClassLoader().getResource(nextImage).toExternalForm()));
        ImageView prev = new ImageView(new Image(this.getClass().getClassLoader().getResource(prevImage).toExternalForm()));
        flagImage = new ImageView(image);
        flagImage.setFitHeight(300);
        flagImage.setFitWidth(200);
        flagImage.setPreserveRatio(true);
        leaderImage = new ImageView(image);
        leaderImage.setFitWidth(200);
        leaderImage.setFitHeight(300);
        leaderImage.setPreserveRatio(true);
        GridPane pane = new GridPane();
        Label name = new Label(bundle.getString("name"));
        Label leader = new Label(bundle.getString("leader"));
        Label capital = new Label(bundle.getString("capital"));
        nameField = new TextField("");
        leaderField = new TextField("");
        capitalField = new TextField("");
        nextButton = new Button();
        prevButton = new Button();
        nextButton.setGraphic(next);
        nextButton.setAlignment(Pos.CENTER_RIGHT);
        nextButton.setOnAction(e -> next());
        prevButton.setOnAction(e -> prev());
        prevButton.setGraphic(prev);
        prevButton.setAlignment(Pos.CENTER_LEFT);
        nextButton.visibleProperty().bind(Bindings.createBooleanBinding((()-> indexProperty.get()<subregions.length-1),indexProperty));
        prevButton.visibleProperty().bind(Bindings.createBooleanBinding((()-> indexProperty.get()>0),indexProperty));



        pane.add(name, 0, 0);
        pane.add(leader, 0, 1);
        pane.add(capital, 0, 2);
        pane.add(nameField, 1, 0);
        pane.add(leaderField, 1, 1);
        pane.add(capitalField, 1, 2);
        borderPane.setLeft(prevButton);
        borderPane.setRight(nextButton);
        pane.setHgap(10);
        pane.setVgap(10);
        hBox.getChildren().addAll(flagImage, pane, leaderImage);
        vBox.getChildren().addAll(borderPane, hBox);
        getDialogPane().setContent(vBox);
        getDialogPane().getButtonTypes().addAll(ButtonType.CLOSE);
        getDialogPane().getStyleClass().add("dialog-pane");

        nameField.textProperty().addListener(e->getImages());
        leaderField.textProperty().addListener(e->getImages());
    }

    private void next() {
        subregions[selectedIndex].nameProperty().unbind();
        subregions[selectedIndex].capitalProperty().unbind();
        subregions[selectedIndex].leaderProperty().unbind();
        selectedIndex++;
        indexProperty.set(selectedIndex);
        nameField.setText(subregions[selectedIndex].getName());
        leaderField.setText(subregions[selectedIndex].getLeader());
        capitalField.setText(subregions[selectedIndex].getCapital());
        subregions[selectedIndex].nameProperty().bind(nameField.textProperty());
        subregions[selectedIndex].leaderProperty().bind(leaderField.textProperty());
        subregions[selectedIndex].capitalProperty().bind(capitalField.textProperty());
        getImages();
    }

    private void prev() {
        subregions[selectedIndex].nameProperty().unbind();
        subregions[selectedIndex].capitalProperty().unbind();
        subregions[selectedIndex].leaderProperty().unbind();
        selectedIndex--;
        indexProperty.set(selectedIndex);
        nameField.setText(subregions[selectedIndex].getName());
        leaderField.setText(subregions[selectedIndex].getLeader());
        capitalField.setText(subregions[selectedIndex].getCapital());
        subregions[selectedIndex].nameProperty().bind(nameField.textProperty());
        subregions[selectedIndex].leaderProperty().bind(leaderField.textProperty());
        subregions[selectedIndex].capitalProperty().bind(capitalField.textProperty());
        getImages();

    }

    public void setSelectedIndex(int index) {
        subregions[selectedIndex].nameProperty().unbind();
        subregions[selectedIndex].capitalProperty().unbind();
        subregions[selectedIndex].leaderProperty().unbind();
        selectedIndex = (index);
        indexProperty.set(selectedIndex);
        nameField.setText(subregions[selectedIndex].getName());
        leaderField.setText(subregions[selectedIndex].getLeader());
        capitalField.setText(subregions[selectedIndex].getCapital());
        subregions[selectedIndex].nameProperty().bind(nameField.textProperty());
        subregions[selectedIndex].leaderProperty().bind(leaderField.textProperty());
        subregions[selectedIndex].capitalProperty().bind(capitalField.textProperty());
        getImages();
    }

    public int getSelectedIndex(){
        return selectedIndex;
    }

    private void getImages() {
        file = new File("work/" + data.getName() + "/" + leaderField.getText() + ".png");
        if (file.exists())
            leaderImage.setImage(new Image(file.toURI().toString()));
        else
            leaderImage.setImage(image);
        file = new File("work/" +  data.getName() + "/"+ nameField.getText()+" Flag.png");
        if(file.exists())
            flagImage.setImage(new Image(file.toURI().toString()));
        else
            flagImage.setImage(image);
    }
}
