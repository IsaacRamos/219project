package MapEditor.view;

import javafx.application.Platform;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.concurrent.Task;
import javafx.scene.control.Dialog;
import javafx.scene.control.ProgressBar;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Created by KaiserFeuer on 7/11/2016.
 */
public class ProgressDialog extends Dialog {
    private final int operations;
    private ProgressBar progressBar;
    SimpleDoubleProperty progression;
    public boolean done;
    public static SimpleIntegerProperty count= new SimpleIntegerProperty(0);


    public ProgressDialog(int operations){
        Stage primary = ((Stage) getDialogPane().getScene().getWindow());
        primary.initStyle(StageStyle.UNDECORATED);
        progression = new SimpleDoubleProperty(0);
        this.operations =operations;
        progressBar = new ProgressBar(0);

        done = true;
        Task task = new Task() {

            @Override
            protected Object call() throws Exception {
                while (!done){
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            updateProgress(count.get(), operations);

                        }
                    });

                    Thread.sleep(10);
                }
                return null;
            }
        };
        new Thread(task).start();
        getDialogPane().setContent(progressBar);

    }
}
