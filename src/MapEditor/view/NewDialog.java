package MapEditor.view;

import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;

import java.io.File;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by Isaac on 6/28/2016.
 */
public class NewDialog extends Dialog {
    private static final String base = "MapEditor.rsc.bundles.MapEditor";
    private static final String style = "MapEditor/rsc/css/Dialog.css";

    private ResourceBundle bundle;

    //CONTROLS
    private TextField nameField;
    private TextField parentField;
    private TextField fileField;
    private DirectoryChooser directoryChooser;
    private Button directoryButton;
    private FileChooser fileChooser;
    private Button fileButton;


    public NewDialog() {

        getDialogPane().getScene().getStylesheets().add(this.getClass().getClassLoader().getResource(style).toExternalForm());
        GridPane pane = new GridPane();
        bundle = ResourceBundle.getBundle(base, Locale.getDefault());
        setTitle(bundle.getString("new-dialog-title"));

        //LABELS
        Label parent = new Label(bundle.getString("parent"));
        Label name = new Label(bundle.getString("name"));
        Label data = new Label(bundle.getString("data"));
        pane.add(parent, 0, 0);
        pane.add(name, 0, 1);
        pane.add(data, 0, 2);

        //CONTROLS
        nameField = new TextField("");
        parentField = new TextField("");
        fileField = new TextField("");
        directoryButton = new Button("...");
        directoryChooser = new DirectoryChooser();
        directoryButton.setOnAction(e->chooseDirectory());
        fileChooser = new FileChooser();
        fileButton = new Button("...");
        fileButton.setAlignment(Pos.BASELINE_LEFT);
        fileButton.setOnAction(e -> chooseFile());

        pane.add(parentField, 1, 0);
        pane.add(directoryButton,2,0);
        pane.add(nameField, 1, 1);
        pane.add(fileField, 1, 2);
        pane.add(fileButton, 2, 2);
        getDialogPane().getButtonTypes().add(ButtonType.OK);
        getDialogPane().getButtonTypes().add(ButtonType.CANCEL);
        //SETTINGS
        pane.setVgap(10);
        pane.setHgap(10);

        getDialogPane().setContent(pane);

    }

    private void chooseFile() {
        File file = fileChooser.showOpenDialog(getDialogPane().getScene().getWindow());
        if (file != null)
            fileField.setText(file.toString());
    }

    private void chooseDirectory() {
        File file = directoryChooser.showDialog(getDialogPane().getScene().getWindow());
        if (file != null) {
            parentField.setText(file.toString());
        }
    }

    public TextField getNameField() {
        return nameField;
    }

    public TextField getParentField() {
        return parentField;
    }

    public TextField getFileField() {
        return fileField;
    }
}
